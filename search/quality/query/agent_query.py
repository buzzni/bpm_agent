import datetime
import time

import requests
import sys
from apscheduler.schedulers.blocking import BlockingScheduler
from bs4 import BeautifulSoup as bsoup



scheduler = BlockingScheduler()

TARGET_DAYS_AGO = 7
# HOUR
TIME_PERIOD = 6


def search_query_agg():
    today = datetime.datetime.today()

    minute_zero_today = datetime.datetime(today.year, today.month, today.day, today.hour)
    with requests.session() as s:
        login_response = s.post("http://has.buzzni.net/login",
                                data={"username": sys.argv[1], "password": sys.argv[2]})

        if login_response.history and login_response.history[0].status_code == 302:
            query_res = s.get(
                "http://has.buzzni.net/stat/trend?graph_id=70&cond_id=505&data_type=pv&date_type=hour&date_num=200")
            df_today = []
            df_yesterday = []
            soup = bsoup(query_res.text, 'html.parser')
            tr = soup.select("#trend-table tr")[2:]
            for td in tr:
                ele = td.find_all("td", recursive=False)
                # 요일, 공백 제거
                date = datetime.datetime.strptime(ele[0].text[:-2], '%Y.%m.%d %H')
                value = int(ele[1].find("span").text.replace(',', ''))
                some_hours_ago = minute_zero_today - datetime.timedelta(hours=TIME_PERIOD)

                if minute_zero_today > date >= some_hours_ago:
                    df_today.append({'value': value, 'date': date})
                    print(value, date)
                if minute_zero_today - datetime.timedelta(
                        TARGET_DAYS_AGO) > date >= some_hours_ago - datetime.timedelta(TARGET_DAYS_AGO):
                    df_yesterday.append({'value': value, 'date': date})
                    print(value, date)
            yesterday_avg = sum(item["value"] for item in df_yesterday) / len(df_yesterday)
            today_avg = sum(item["value"] for item in df_today) / len(df_today)
            # 전날 값 까지 넣으면됌

            print(TARGET_DAYS_AGO, "일 전의 +20%: " + str(yesterday_avg * 1.2))
            print(TARGET_DAYS_AGO, "일 전의 -20%: " + str(yesterday_avg * 0.8))
            print(TARGET_DAYS_AGO, "일 전", yesterday_avg, ",", "오늘", today_avg)

            percent_value = 100 - (yesterday_avg / today_avg * 100)

            print(percent_value, '%')
            yesterday_str = datetime.datetime.strftime(
                datetime.datetime.today() - datetime.timedelta(TARGET_DAYS_AGO), '%Y-%m-%d')
            log = {
                "key_name": "hsmoa.search.query_abnormal",
                "value": abs(percent_value),
                "host": "11층",
                "description": "최근 " + str(TIME_PERIOD) + "시간의 쿼리가 " +
                               str(TARGET_DAYS_AGO) + "일 전(" + yesterday_str + ", " +
                               str(round(yesterday_avg, 2)) + ")에 비해 " +
                               str(round(percent_value, 2)) + "% (" +
                               str(round(today_avg, 2)) + ") 만큼 변화했습니다."
            }
            headers = {'Content-Type': 'application/json'}
            requests.post("https://bpm.buzzni.net/api/v1/logs/", headers=headers, json=log)

        else:
            print("HAS login fail")


def main():
    print("Search Query Agent is running ...")

    scheduler.add_job(func=search_query_agg, trigger='cron', hour='9-22', minute=1)
    # scheduler.add_job(func=search_query_agg, trigger='cron', day_of_week='mon-fri', minute='1-59', second='5/8')
    scheduler.start()
    while True:
        time.sleep(30)
        print(datetime.datetime.today())


if __name__ == '__main__':
    print(sys.argv[1], sys.argv[2])
    main()
