import datetime
import time

import requests
from apscheduler.schedulers.background import BackgroundScheduler
from bs4 import BeautifulSoup as bsoup

scheduler = BackgroundScheduler()

# HOUR
TIME_PERIOD = 6


def get_all_search(req_session, urlpath, today):
    minute_zero_today = datetime.datetime(today.year, today.month, today.day, today.hour)
    query_res = req_session.get(urlpath)
    df_today = []

    soup = bsoup(query_res.text, 'html.parser')
    tr = soup.select("#trend-table tr")[2:]
    for td in tr:
        ele = td.find_all("td", recursive=False)
        # 요일, 공백 제거
        date = datetime.datetime.strptime(ele[0].text[:-2], '%Y.%m.%d %H')
        value = int(ele[1].find("span").text.replace(',', ''))
        some_hours_ago = minute_zero_today - datetime.timedelta(hours=TIME_PERIOD)

        if minute_zero_today > date >= some_hours_ago:
            df_today.append({'value': value, 'date': date})

    today_all_avg = sum(item["value"] for item in df_today) / len(df_today)
    return today_all_avg


def search_relative():
    today = datetime.datetime.today()

    with requests.session() as s:
        login_response = s.post("http://has.buzzni.net/login",
                                data={"username": "buzzni", "password": "hsmoa2017"})

        if login_response.history and login_response.history[0].status_code == 302:
            today_all_avg = get_all_search(s,
                                           "http://has.buzzni.net/stat/trend?graph_id=70&cond_id=505&data_type=pv&date_type=hour&date_num=50",
                                           today)
            today_relative_avg = get_all_search(s,
                                                "http://has.buzzni.net/stat/trend?graph_id=95&cond_id=457&data_type=pv&date_type=hour&date_num=50",
                                                today)

            # 전날 값 까지 넣으면됌

            print("총검색량:", today_all_avg)
            print("연관검색어:", today_relative_avg)
            ratio = today_relative_avg / today_all_avg

            percent_value = ratio * 100
            print("ratio:", ratio)

            log = {
                "key_name": "hsmoa.search.query_relative_ratio",
                "value": round(percent_value, 2),
                "host": "11층",
                "description": "최근 " + str(TIME_PERIOD) + "시간의 쿼리(" + str(round(today_all_avg, 2)) + ") 대비 " +
                               "연관검색어(" + str(round(today_relative_avg, 2)) + ")의 비율이 " +
                               str(round(percent_value, 2)) + "% 입니다."
            }
            headers = {'Content-Type': 'application/json'}
            requests.post("https://bpm.buzzni.net/api/v1/logs/", headers=headers, json=log)
            print(log)

        else:
            print("HAS login fail")


def main():
    print("Search Query-Relative Ratio Agent is running ...")

    scheduler.add_job(func=search_relative, trigger='cron', hour='9-23', minute=1)
    scheduler.start()
    # search_relative()
    while True:
        time.sleep(30)
        print(datetime.datetime.today())


if __name__ == '__main__':
    main()
